package org.but.feec.carrental.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.carrental.api.PersonDeleteView;
import org.but.feec.carrental.api.PersonBasicView;
import org.but.feec.carrental.api.PersonEditView;
import org.but.feec.carrental.data.PersonRepository;
import org.but.feec.carrental.service.PersonService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class PersonDeleteController {
    private static final Logger logger = LoggerFactory.getLogger(PersonDeleteController.class);

    @FXML
    public Button deletePersonButton;

    @FXML
    public TextField deletePersonId;

    @FXML
    public TextField deletePersonName;

    @FXML
    public TextField deletePersonModel;

    @FXML
    public TextField deletePersonColor;

    private PersonService personService;
    private PersonRepository personRepository;
    private ValidationSupport validation;
    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        personRepository = new PersonRepository();
        personService = new PersonService(personRepository);

        validation = new ValidationSupport();
        validation.registerValidator(deletePersonId, Validator.createEmptyValidator("The id can not be empty."));
        deletePersonId.setEditable(false);
        validation.registerValidator(deletePersonName, Validator.createEmptyValidator(("The name of car brand can not be empty.")));
        deletePersonName.setEditable(false);
        validation.registerValidator(deletePersonModel, Validator.createEmptyValidator("The name of car model can not be empty."));
        deletePersonModel.setEditable(false);
        validation.registerValidator(deletePersonColor, Validator.createEmptyValidator("The color of can not be empty."));
        deletePersonColor.setEditable(false);


        deletePersonButton.disableProperty().bind(validation.invalidProperty());

        loadPersonsData();

        logger.info("PersonEditController initalized");
    }

    private void loadPersonsData(){
        Stage stage = this.stage;
        if (stage.getUserData() instanceof PersonBasicView){
            PersonBasicView personBasicView = (PersonBasicView) stage.getUserData();
            deletePersonId.setText(String.valueOf(personBasicView.getId()));
            deletePersonName.setText(String.valueOf(personBasicView.getName()));
            deletePersonModel.setText(String.valueOf(personBasicView.getModel()));
            deletePersonColor.setText(String.valueOf(personBasicView.getColor()));
        }
    }

    public void handleDeletePersonButton(ActionEvent event) {
        Long id = Long.valueOf(deletePersonId.getText());
        String name = deletePersonName.getText();
        String model = deletePersonModel.getText();
        String color = deletePersonColor.getText();

        PersonDeleteView personDeleteView = new PersonDeleteView();
        personDeleteView.setId(id);
        personDeleteView.setName(name);
        personDeleteView.setModel(model);
        personDeleteView.setColor(color);

        personService.deletePerson(personDeleteView);

        personEditedConfirmationDialog();
    }

    private void personEditedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Car Deleted Confirmation");
        alert.setHeaderText("This was successfully deleted.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }

}
