package org.but.feec.carrental.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.util.Duration;
import org.but.feec.carrental.api.PersonCreateView;  //toto edituju
import org.but.feec.carrental.data.PersonRepository; //toto edituju
import org.but.feec.carrental.service.PersonService; //toto edituju
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class PersonCreateController {
    private static final Logger logger = LoggerFactory.getLogger(PersonCreateController.class);

    @FXML
    public Button newPersonCreateCar;

    @FXML
    public TextField newPersonColor;

    @FXML
    public TextField newPersonName;

    @FXML
    public TextField newPersonModel;

    private PersonService personService;
    private PersonRepository personRepository;
    private ValidationSupport validation;

    @FXML
    public void initialize() {
        personRepository = new PersonRepository();
        personService = new PersonService(personRepository);

        validation = new ValidationSupport();
        validation.registerValidator(newPersonColor, Validator.createEmptyValidator("The color can not be empty."));
        validation.registerValidator(newPersonName, Validator.createEmptyValidator("The name of car manufacture can not be empty."));
        validation.registerValidator(newPersonModel, Validator.createEmptyValidator("The name of model can not be empty."));

        newPersonCreateCar.disableProperty().bind(validation.invalidProperty());

        logger.info("PersonCreateController initialized");
    }

    @FXML
    void handleCreateNewPerson(ActionEvent event) {
        String color = newPersonColor.getText();
        String name = newPersonName.getText();
        String model = newPersonModel.getText();

        PersonCreateView personCreateView = new PersonCreateView();
        personCreateView.setColor(color);
        personCreateView.setName(name);
        personCreateView.setModel(model);

        personService.createPerson(personCreateView);

        personCreatedConfirmationDialog();
    }

    private void personCreatedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Car Created Confirmation");
        alert.setHeaderText("Your car was successfully created.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }


}
