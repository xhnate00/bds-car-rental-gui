package org.but.feec.carrental.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.but.feec.carrental.api.PersonFilterView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PersonSearchController {

    private static final Logger logger = LoggerFactory.getLogger(PersonSearchController.class);

    @FXML
    public TextField searchCarName;

    @FXML
    public TextField searchCarModel;

    @FXML
    public TextField searchCarColor;

    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        searchCarName.setEditable(false);
        searchCarModel.setEditable(false);
        searchCarColor.setEditable(false);

        loadPersonsData();

        logger.info("PersonSearchController initialized");
    }

    private void loadPersonsData()
    {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof PersonFilterView)
        {
            PersonFilterView personBasicView = (PersonFilterView) stage.getUserData();
            searchCarName.setText(String.valueOf(personBasicView.getName()));
            searchCarModel.setText(String.valueOf(personBasicView.getModel()));
            searchCarColor.setText(String.valueOf(personBasicView.getColor()));

        }
    }


}