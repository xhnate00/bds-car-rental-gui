package org.but.feec.carrental.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.but.feec.carrental.api.PersonDetailView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PersonDetailViewController {
    private static final Logger logger = LoggerFactory.getLogger(PersonDetailViewController.class);

    @FXML
    private TextField nameTextField;
    @FXML
    private TextField modelTextField;
    @FXML
    private TextField colorTextField;
    @FXML
    private TextField countryTextField;
    @FXML
    private TextField licencePlateTextField;

    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        //idTextField.setEditable(false);
        nameTextField.setEditable(false);
        modelTextField.setEditable(false);
        colorTextField.setEditable(false);
        countryTextField.setEditable(false);
        licencePlateTextField.setEditable(false);


        loadPersonsData();
        logger.info("PersonsDetailViewController initialized");

    }

    private void loadPersonsData()
    {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof PersonDetailView)
        {
            PersonDetailView personBasicView = (PersonDetailView) stage.getUserData();
            //idTextField.setText(String.valueOf(personBasicView.getId()));
            nameTextField.setText(String.valueOf(personBasicView.getName()));
            modelTextField.setText(String.valueOf(personBasicView.getModel()));
            colorTextField.setText(String.valueOf(personBasicView.getColor()));
            countryTextField.setText(String.valueOf(personBasicView.getCountry()));
            licencePlateTextField.setText(String.valueOf(personBasicView.getLicencePlate()));

        }
    }

}
