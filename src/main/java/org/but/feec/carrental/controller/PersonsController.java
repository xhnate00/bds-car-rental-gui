package org.but.feec.carrental.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.but.feec.carrental.App;
import org.but.feec.carrental.api.PersonBasicView;
import org.but.feec.carrental.api.PersonDetailView;
import org.but.feec.carrental.api.PersonFilterView;
import org.but.feec.carrental.data.PersonRepository;
import org.but.feec.carrental.exceptions.ExceptionHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import org.but.feec.carrental.service.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class PersonsController {
    @FXML
    public Button sqlInjectionButton;

    @FXML
    public Button searchCarButton;

    @FXML
    public Button addCarButton;

    @FXML
    public Button refreshButton;

    @FXML
    public TextField searchId;

    @FXML
    private TableColumn<PersonBasicView, Long> carId;

    @FXML
    private TableColumn<PersonBasicView, String> carName;

    @FXML
    private TableColumn<PersonBasicView, String> carModel;

    @FXML
    private TableColumn<PersonBasicView, String> carColor;

    @FXML
    private TableView<PersonBasicView> systemCarsTableView;

    private static final Logger logger = LoggerFactory.getLogger(PersonsController.class);

    public PersonsController() {
    }

    private PersonService personService;
    private PersonRepository personRepository;

    @FXML
    private void initialize(){
        personRepository = new PersonRepository();
        personService = new PersonService(personRepository);

        carId.setCellValueFactory(new PropertyValueFactory<PersonBasicView, Long>("id"));
        carName.setCellValueFactory(new PropertyValueFactory<PersonBasicView, String>("name"));
        carModel.setCellValueFactory(new PropertyValueFactory<PersonBasicView, String>("model"));
        carColor.setCellValueFactory(new PropertyValueFactory<PersonBasicView, String>("color"));

        ObservableList<PersonBasicView> observablePersonsList = initializePersonsData();
        systemCarsTableView.setItems(observablePersonsList);

        systemCarsTableView.getSortOrder().add(carId);

        initializeTableViewSelection();


        logger.info("PersonsController initialized");
    }

    private ObservableList<PersonBasicView> initializePersonsData() {
        List<PersonBasicView> persons = personService.getPersonsBasicView();
        return FXCollections.observableArrayList(persons);
    }


    public void handleExitMenuItem(ActionEvent event) {
        System.exit(0);
    }

    private void initializeTableViewSelection() {
        MenuItem edit = new MenuItem("Edit car");
        MenuItem detailedView = new MenuItem("Detailed car view");
        MenuItem delete = new MenuItem("Delete car");
        edit.setOnAction((ActionEvent event) -> {
            PersonBasicView personView = systemCarsTableView.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(App.class.getResource("fxml/PersonEdit.fxml"));
                Stage stage = new Stage();
                stage.setUserData(personView);
                stage.setTitle("CAR RENTAL");

                PersonsEditController controller = new PersonsEditController();
                controller.setStage(stage);
                fxmlLoader.setController(controller);
                stage.getIcons().add(new Image(App.class.getResourceAsStream("logos/logo.png")));

                Scene scene = new Scene(fxmlLoader.load(), 600, 500);

                stage.setScene(scene);

                stage.show();
            } catch (IOException ex) {
                ExceptionHandler.handleException(ex);
            }
        });

        detailedView.setOnAction((ActionEvent event) -> {
            PersonBasicView personView = systemCarsTableView.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(App.class.getResource("fxml/PersonDetailView.fxml"));
                Stage stage = new Stage();

                Long personId = personView.getId();
                PersonDetailView personDetailView = personService.getPersonDetailView(personId);

                stage.setUserData(personDetailView);
                stage.setTitle("CAR RENTAL");
                stage.getIcons().add(new Image(App.class.getResourceAsStream("logos/logo.png")));

                PersonDetailViewController controller = new PersonDetailViewController();
                controller.setStage(stage);
                fxmlLoader.setController(controller);

                Scene scene = new Scene(fxmlLoader.load(), 600, 500);

                stage.setScene(scene);

                stage.show();
            } catch (IOException ex) {
                ExceptionHandler.handleException(ex);
            }
        });

        delete.setOnAction((ActionEvent event) ->{
            PersonBasicView personView = systemCarsTableView.getSelectionModel().getSelectedItem();
            try{
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(App.class.getResource("fxml/PersonDelete.fxml"));
                Stage stage = new Stage();
                stage.setUserData(personView);
                stage.setTitle("CAR RENTAL");

                PersonDeleteController controller = new PersonDeleteController();
                controller.setStage(stage);
                fxmlLoader.setController(controller);

                Scene scene = new Scene(fxmlLoader.load(), 600, 500);

                stage.setScene(scene);

                stage.show();
            }
            catch(IOException ex){
                ExceptionHandler.handleException(ex);
            }
        });


        ContextMenu menu = new ContextMenu();
        menu.getItems().add(edit);
        menu.getItems().addAll(detailedView);
        menu.getItems().add(delete);
        systemCarsTableView.setContextMenu(menu);
    }

    public void handleRefreshButton(ActionEvent actionEvent) {
        ObservableList<PersonBasicView> observablePersonsList = initializePersonsData();
        systemCarsTableView.setItems(observablePersonsList);
        systemCarsTableView.refresh();
        systemCarsTableView.sort();
    }

    public void handleAddCarButton(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/PersonCreate.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 500);
            Stage stage = new Stage();
            stage.setTitle("CAR RENTAL");
            stage.setScene(scene);
            stage.getIcons().add(new Image(App.class.getResourceAsStream("logos/logo.png")));
            stage.show();
        } catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }
    }

    public void handleSearchCarButton(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/PersonSearch.fxml"));
            Stage stage = new Stage();

            Long personId = Long.valueOf(searchId.getText());
            PersonFilterView personFilterView = personService.getPersonFilterView(personId);

            stage.setUserData(personFilterView);
            stage.setTitle("CAR RENTAL");
            stage.getIcons().add(new Image(App.class.getResourceAsStream("logos/logo.png")));

            PersonSearchController controller = new PersonSearchController();
            controller.setStage(stage);
            fxmlLoader.setController(controller);

            Scene scene = new Scene(fxmlLoader.load(), 600, 500);

            stage.setScene(scene);

            stage.show();
        } catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }

    }

    public void handleSqlInjectionButton(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/SqlInjection.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 500);
            Stage stage = new Stage();
            stage.setTitle("CAR RENTAL");
            stage.getIcons().add(new Image(App.class.getResourceAsStream("logos/logo.png")));
            stage.setScene(scene);

            stage.show();
        } catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }
    }

}
