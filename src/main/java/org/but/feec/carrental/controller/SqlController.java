package org.but.feec.carrental.controller;

import org.but.feec.carrental.api.PersonBasicView;
import org.but.feec.carrental.api.SqlView;
import org.but.feec.carrental.config.DataSourceConfig;
import org.but.feec.carrental.exceptions.DataAccessException;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SqlController {
    private static final Logger logger = LoggerFactory.getLogger(SqlController.class);

    @FXML
    public Button createButton;

    @FXML
    public Button refreshButton;

    @FXML
    public Button runButton;

    @FXML
    public TableColumn<SqlView, String> c1;

    @FXML
    public TableColumn<SqlView, String> c2;

    @FXML
    public TableColumn<SqlView, String> c3;

    @FXML
    public TableColumn<SqlView, String> c4;

    @FXML
    public TableView<SqlView> sqlTableView;

    @FXML
    public TextField sqlTextField;

    @FXML
    private void initialize() {
        c1.setCellValueFactory(new PropertyValueFactory<SqlView, String>("c1"));
        c2.setCellValueFactory(new PropertyValueFactory<SqlView, String>("c2"));
        c3.setCellValueFactory(new PropertyValueFactory<SqlView, String>("c3"));
        c4.setCellValueFactory(new PropertyValueFactory<SqlView, String>("c4"));

        ObservableList<SqlView> observablePersonsList = initializeSqlData();
        sqlTableView.setItems(observablePersonsList);

        logger.info("SqlController initialized");

    }


    private SqlView mapToSqlView(ResultSet rs) throws SQLException {
        SqlView sqlView = new SqlView();
        sqlView.setC1(rs.getString("c1"));
        sqlView.setC2(rs.getString("c2"));
        sqlView.setC3(rs.getString("c3"));
        sqlView.setC4(rs.getString("c4"));

        return sqlView;
    }

    public List<SqlView> getSqlView() {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     " SELECT c1, c2, c3, c4 " +
                             " FROM public.sql_table");
             ResultSet resultSet = preparedStatement.executeQuery()) {
            List<SqlView> sqlBasicViews = new ArrayList<>();
            while (resultSet.next()) {
                sqlBasicViews.add(mapToSqlView(resultSet));
            }
            return sqlBasicViews;
        } catch (SQLException e) {
            throw new DataAccessException("SQL basic view could not be loaded.", e);
        }
    }

    private ObservableList<SqlView> initializeSqlData() {
        List<SqlView> rows = getSqlView();
        return FXCollections.observableArrayList(rows);
    }

    public void handleSqlCreate(ActionEvent actionEvent) {

        String sql_query = "CREATE TABLE IF NOT EXISTS public.sql_table" +
                " (sql_id SERIAL NOT NULL PRIMARY KEY, c1 VARCHAR, c2 VARCHAR, c3 VARCHAR, c4 VARCHAR);";
        try (Connection connection = DataSourceConfig.getConnection()){
            connection.createStatement().executeUpdate(sql_query);
            handleSqlInsert();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void handleSqlInsert() throws SQLException {
        String sql_query = "INSERT INTO public.sql_table (c1, c2, c3, c4) " +
                "VALUES ('kvit', 'vykoledovaných', 'afinitního', 'spodním'), ('posmívalo', 'šperkařství', 'iluzionisté', 'mimózou'), ('neprodiskutovány', 'kalhotového', 'jasnovidného', 'vysrážely'), ('nevrážejme', 'asketických', 'zaizolujete', 'padavost');";
        try (Connection connection = DataSourceConfig.getConnection()){
            connection.createStatement().executeUpdate(sql_query);
            tableCreatedConfirmationDialog();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void tableCreatedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Table Created Confirmation");
        alert.setHeaderText("SQL table was successfully created.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }

    public void handleSqlQuery(ActionEvent actionEvent) {
        String parameter = sqlTextField.getText();

        String sql_query = "SELECT c1, c2, c3, c4 FROM public.sql_table WHERE c1 = '"+parameter+"';";
        try (Connection connection = DataSourceConfig.getConnection()){

            List<SqlView> sqlBasicViews = new ArrayList();
            ResultSet rs = connection.createStatement().executeQuery(sql_query);

            while (rs.next()) {
                sqlBasicViews.add(mapToSqlView(rs));
            }

            sqlTableView.setItems(FXCollections.observableArrayList(sqlBasicViews));

        } catch (SQLException e) {
            throw new DataAccessException("SQL query could not be performed.", e);
        }
    }

    public void handleRefresh(ActionEvent actionEvent) {
        ObservableList<SqlView> observablePersonsList = initializeSqlData();
        sqlTableView.setItems(observablePersonsList);
        sqlTableView.setItems(null);
        sqlTableView.refresh();
        sqlTableView.sort();
    }
}
