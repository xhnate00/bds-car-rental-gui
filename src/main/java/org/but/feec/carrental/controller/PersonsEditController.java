package org.but.feec.carrental.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.carrental.api.PersonBasicView;
import org.but.feec.carrental.api.PersonEditView;
import org.but.feec.carrental.data.PersonRepository;
import org.but.feec.carrental.service.PersonService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;


public class PersonsEditController {
    private static final Logger logger = LoggerFactory.getLogger(PersonsEditController.class);

    @FXML
    public Button editCarButton;
    @FXML
    public TextField idTextField;
    @FXML
    public TextField nameTextField;
    @FXML
    public TextField modelTextField;
    @FXML
    public TextField colorTextField;

    private PersonService personService;
    private PersonRepository personRepository;
    private ValidationSupport validation;
    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        personRepository = new PersonRepository();
        personService = new PersonService(personRepository);

        validation = new ValidationSupport();
        validation.registerValidator(idTextField, Validator.createEmptyValidator("The id must not be empty."));
        idTextField.setEditable(false);
        validation.registerValidator(nameTextField, Validator.createEmptyValidator(("The name of car brand can not be empty.")));
        validation.registerValidator(modelTextField, Validator.createEmptyValidator("The name of car model can not be empty."));
        validation.registerValidator(colorTextField, Validator.createEmptyValidator("The color of car can not be empty."));


        editCarButton.disableProperty().bind(validation.invalidProperty());

        loadPersonsData();

        logger.info("PersonEditController initalized");
    }

    private void loadPersonsData(){
        Stage stage = this.stage;
        if (stage.getUserData() instanceof PersonBasicView){
            PersonBasicView personBasicView = (PersonBasicView) stage.getUserData();
            idTextField.setText(String.valueOf(personBasicView.getId()));
            nameTextField.setText(String.valueOf(personBasicView.getName()));
            modelTextField.setText(String.valueOf(personBasicView.getModel()));
            colorTextField.setText(String.valueOf(personBasicView.getColor()));
        }
    }

    public void handleEditPersonButton(ActionEvent event) {
        Long id = Long.valueOf(idTextField.getText());
        String name = nameTextField.getText();
        String model = modelTextField.getText();
        String color = colorTextField.getText();

        PersonEditView personEditView = new PersonEditView();
        personEditView.setId(id);
        personEditView.setName(name);
        personEditView.setModel(model);
        personEditView.setColor(color);

        personService.editPerson(personEditView);

        personEditedConfirmationDialog();
    }

    private void personEditedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Person Edited Confirmation");
        alert.setHeaderText("Your person was successfully edited.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }


}