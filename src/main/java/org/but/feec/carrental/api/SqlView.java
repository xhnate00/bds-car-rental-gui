package org.but.feec.carrental.api;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SqlView {
    private StringProperty c1 = new SimpleStringProperty();
    private StringProperty c2 = new SimpleStringProperty();
    private StringProperty c3 = new SimpleStringProperty();
    private StringProperty c4 = new SimpleStringProperty();

    public String getC1() { return c1.get(); }

    public void setC1(String c1) { this.c1.set(c1); }

    public String getC2() { return c2.get(); }

    public void setC2(String c2) { this.c2.set(c2); }

    public String getC3() { return c3.get(); }

    public void setC3(String c3) { this.c3.set(c3); }

    public String getC4() { return c4.get(); }

    public void setC4(String c4) { this.c4.set(c4); }



    public StringProperty c1Property() { return c1; }
    public StringProperty c2Property() { return c2; }
    public StringProperty c3Property() { return c3; }
    public StringProperty c4Property() { return c4; }
}
