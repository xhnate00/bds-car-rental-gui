package org.but.feec.carrental.api;

import java.util.Arrays;

public class PersonCreateView {
    private String name;
    private String model;
    private String color;

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getModel() { return model; }

    public void setModel(String model) { this.model = model; }

    public String getColor() { return color; }

    public void setColor(String color) { this.color = color; }


    @Override
    public String toString() {
        return "PersonCreateView{" +
                "color='" + color + '\'' +
                ", name='" + name + '\'' +
                ", model='" + model + '\'' +
                '}';
    }
}