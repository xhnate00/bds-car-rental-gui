package org.but.feec.carrental.api;

public class PersonEditView {
    private Long id;
    private String name;
    private String model;
    private String color;


    public Long getId() { return id; }

    public void setId(Long id) { this.id=id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getModel() { return model; }

    public void setModel(String model) { this.model = model; }

    public String getColor() { return color; }

    public void setColor(String color) { this.color = color; }



    @Override
    public String toString() {
        return "PersonEditView{" +
                "name='" + name + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                '}';
    }

}