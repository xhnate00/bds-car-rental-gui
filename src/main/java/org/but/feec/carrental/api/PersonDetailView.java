package org.but.feec.carrental.api;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PersonDetailView {
    private LongProperty id = new SimpleLongProperty();
    private StringProperty name = new SimpleStringProperty();
    private StringProperty model = new SimpleStringProperty();
    private StringProperty color = new SimpleStringProperty();
    private StringProperty country = new SimpleStringProperty();
    private StringProperty licencePlate = new SimpleStringProperty();

    public Long getId() {
        return idProperty().get();
    }

    public void setId(Long id) {
        this.idProperty().setValue(id);
    }

    public String getName() {
        return nameProperty().get();
    }

    public void setName(String name) {
        this.nameProperty().setValue(name);
    }

    public String getModel() {
        return modelProperty().get();
    }

    public void setModel(String model) {
        this.modelProperty().setValue(model);
    }

    public String getColor() {
        return colorProperty().get();
    }

    public void setColor(String color) {
        this.colorProperty().setValue(color);
    }

    public String getCountry() {
        return countryProperty().get();
    }

    public void setCountry(String country) {
        this.countryProperty().set(country);
    }

    public String getLicencePlate() {
        return licencePlateProperty().get();
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlateProperty().setValue(licencePlate);
    }



    public LongProperty idProperty() {
        return id;
    }

    public StringProperty nameProperty() {
        return name;
    }

    public StringProperty modelProperty() {
        return model;
    }

    public StringProperty colorProperty() {
        return color;
    }

    public StringProperty countryProperty() {
        return country;
    }

    public StringProperty licencePlateProperty() {
        return licencePlate;
    }


}
