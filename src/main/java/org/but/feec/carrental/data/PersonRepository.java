package org.but.feec.carrental.data;

import org.but.feec.carrental.api.*;
import org.but.feec.carrental.config.DataSourceConfig;
import org.but.feec.carrental.exceptions.DataAccessException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonRepository {
    public PersonAuthView findPersonByEmail(String email) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT email, pwd" +
                             " FROM public.person p" +
                             " WHERE p.email = ?")
        ) {
            preparedStatement.setString(1, email);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToPersonAuth(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find person by ID with addresses failed.", e);
        }
        return null;
    }

    private PersonAuthView mapToPersonAuth(ResultSet rs) throws SQLException {
        PersonAuthView person = new PersonAuthView();
        person.setEmail(rs.getString("email"));
        person.setPassword(rs.getString("pwd"));
        return person;
    }



    public List<PersonBasicView> getPersonsBasicView() {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT brand_id, name, model, color" +
                             " FROM public.brand p");
             ResultSet resultSet = preparedStatement.executeQuery();) {
            List<PersonBasicView> personBasicViews = new ArrayList<>();
            while (resultSet.next()) {
                personBasicViews.add(mapToPersonBasicView(resultSet));
            }
            return personBasicViews;
        } catch (SQLException e) {
            throw new DataAccessException("Persons basic view could not be loaded.", e);
        }
    }

    public PersonDetailView findPersonDetailedView(Long carId) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT a.name, a.model, a.color, a.country, p.licence_plate" +
                             " FROM car p" +
                             " inner JOIN brand a ON p.brand_id = a.brand_id" +
                             " where p.brand_id = ?")
        ) {
            preparedStatement.setLong(1, carId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToPersonDetailView(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find car by ID with addresses failed.", e);
        }
        return null;
    }

    public PersonFilterView findPersonFilteredView(Long carId) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT name, model, color" +
                             " FROM public.brand p" +
                             " WHERE p.brand_id = ?")
        ) {
            preparedStatement.setLong(1, carId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToPersonFilterView(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find car by ID with addresses failed.", e);
        }
        return null;
    }

    private PersonFilterView mapToPersonFilterView(ResultSet rs) throws SQLException {
        PersonFilterView personFilterView = new PersonFilterView();
        personFilterView.setName(rs.getString("name"));
        personFilterView.setModel(rs.getString("model"));
        personFilterView.setColor(rs.getString("color"));
        return personFilterView;
    }

    private PersonBasicView mapToPersonBasicView(ResultSet rs) throws SQLException {
        PersonBasicView personBasicView = new PersonBasicView();
        personBasicView.setId(rs.getLong("brand_id"));
        personBasicView.setName(rs.getString("name"));
        personBasicView.setModel(rs.getString("model"));
        personBasicView.setColor(rs.getString("color"));
        return personBasicView;
    }

    private PersonDetailView mapToPersonDetailView(ResultSet rs) throws SQLException {
        PersonDetailView personDetailView = new PersonDetailView();
        //personDetailView.setId(rs.getLong("id"));
        personDetailView.setName(rs.getString("name"));
        personDetailView.setModel(rs.getString("model"));
        personDetailView.setColor(rs.getString("color"));
        personDetailView.setCountry(rs.getString("country"));
        personDetailView.setLicencePlate(rs.getString("licence_plate"));
        return personDetailView;
    }

    public void editPerson(PersonEditView personEditView) {
        String insertPersonSQL = "UPDATE public.brand p " +
                                "SET name = ?, model = ?, color = ? " +
                                "WHERE p.brand_id = ?";
        String checkIfExists = "SELECT model " +
                                "FROM public.brand p " +
                                "WHERE p.brand_id = ?";
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(insertPersonSQL, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, personEditView.getModel());
            preparedStatement.setString(2, personEditView.getName());
            preparedStatement.setString(3, personEditView.getColor());
            preparedStatement.setLong(4, personEditView.getId());

            try {
                connection.setAutoCommit(false);
                try (PreparedStatement ps = connection.prepareStatement(checkIfExists, Statement.RETURN_GENERATED_KEYS)) {
                    ps.setLong(1, personEditView.getId());
                    ps.execute();
                } catch (SQLException e) {
                    throw new DataAccessException("This car for edit do not exists.");
                }

                int affectedRows = preparedStatement.executeUpdate();

                if (affectedRows == 0) {
                    throw new DataAccessException("Creating car failed, no rows affected.");
                }
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
            } finally {
                connection.setAutoCommit(true);
            }
        } catch (SQLException e) {
            throw new DataAccessException("Creating car failed operation on the database failed.");
        }
    }

    public void createPerson(PersonCreateView personCreateView) {
        String insertPersonSQL = "INSERT INTO brand (color, name, model) " +
                                "VALUES (?, ?, ?)";
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(insertPersonSQL, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, personCreateView.getColor());
            preparedStatement.setString(2, personCreateView.getName());
            preparedStatement.setString(3, personCreateView.getModel());

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new DataAccessException("Creating car failed, no rows affected.");
            }
        } catch (SQLException e) {
            throw new DataAccessException("Creating car failed operation on the database failed.");
        }
    }

    public void deletePerson(PersonDeleteView personDeleteView) {
        String insertPersonSQL = "DELETE FROM public.brand p " +
                                "WHERE p.brand_id = ?";
        String checkIfExists = "SELECT model " +
                            "FROM public.brand p " +
                            "WHERE p.brand_id = ?";
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(insertPersonSQL, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setLong(1, personDeleteView.getId());

            try {
                connection.setAutoCommit(false);
                try (PreparedStatement ps = connection.prepareStatement(checkIfExists, Statement.RETURN_GENERATED_KEYS)) {
                    ps.setLong(1, personDeleteView.getId());
                    ps.execute();
                } catch (SQLException e) {
                    throw new DataAccessException("This car for delete do not exists.");
                }

                int affectedRows = preparedStatement.executeUpdate();

                if (affectedRows == 0) {
                    throw new DataAccessException("Delete of car failed, no rows affected.");
                }
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
            } finally {
                connection.setAutoCommit(true);
            }
        } catch (SQLException e) {
            throw new DataAccessException("Delete of car failed, operation on the database failed.");
        }
    }


}
